import smtplib

me = "edinburgh.rosoc@gmail.com"
you = ["r.c.oprescu@sms.ed.ac.uk"]

def readFile(fileName):
    f = open(fileName, "r")
    people = f.read()
    f.close
    return people
 
def getMenteeDescription(mentee):
    text = "<h3>Mentee Description:\n</h3>"
    text += "<p><b>Name</b>: %s<br>\n" %mentee[0]
    text += "<b>Degree</b>: %s<br>\n" %mentee[1]
    text += "<b>Motivation</b>: %s<br>\n" %mentee[2]
    text += "<b>Email</b>: %s<br>\n" %mentee[3] 
    text += "<b>Hobbies</b>: %s<br>\n" %mentee[4]
    text += "<b>Home Town</b>: %s<br>\n" %mentee[5]
    text += "<b>High School</b>: %s<br>\n</p>" %mentee[6]
    return text

def getMentorDescription(mentor):
    text = "<h3>Mentor Description:\n</h3>"
    text += "<p><b>Name</b>: %s<br>\n" %mentor[0]
    text += "<b>University</b>: %s<br>\n" %mentor[1]
    text += "<b>Degree</b>: %s<br>\n" %mentor[2]
    text += "<b>Year</b>: %s<br>\n" %mentor[3]
    text += "<b>Email</b>: %s<br>\n" %mentor[4]
    text += "<b>Hobbies</b>: %s<br>\n" %mentor[5]
    text += "<b>Current Job</b>: %s<br>\n" %mentor[6]
    text += "<b>Home town</b>: %s<br></p>\n" %mentor[7]
    return text

def sendEmail(mentee, mentor):
    emailMentee = mentee[3]
    emailMentor = mentor[4]
    content = "From: EU RoSoc<%s>\n" %me
    content += "To: %s; %s; %s\n" % (emailMentee, emailMentor, you[0])
    content += "MIME-Version: 1.0\n"
    content += "Content-type: text/html\n"
    content += "Subject: Mentorship Programme Matching Mentee - Mentor\n"
    content += "<h3>Thank you very much for participating in our mentorship program.</h3>\n"
    content += "<p>We are happy to announce that you are one of the first persons to be matched.\n</p>"
    content += "<p>Please find attached the details of your mentor/ mentee and start chatting.\n</p>"
    content += getMenteeDescription(mentee)
    content += getMentorDescription(mentor)
    content += "<br>"
    
    content += "<p>Thank you,\n</p><p>EU RoSoc Committee\n</p>"
    
    toEmails = [you[0], emailMentee, emailMentor]
    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(me, toEmails, content)         
        print "Successfully sent email"
    except Exception:
        print "Error: unable to send email"
     

def main():
    mentees = readFile("mentees.txt")
    mentors = readFile("mentors.txt")
    
    mentees = mentees.splitlines()
    mentors = mentors.splitlines()
    
    menteesMatrix = []
    menteesLine = []
    for line in mentees:
        if line == line.upper():
            menteesMatrix.append(menteesLine)
            menteesLine = []
        else:
            menteesLine.append(line.rstrip())
    
    mentorsMatrix = []
    mentorsLine = []
    for line in mentors:
        if line == line.upper():
            mentorsMatrix.append(mentorsLine)
            mentorsLine = []
        else:
            mentorsLine.append(line.rstrip())
    
    print menteesMatrix
    for mentee, mentor in zip(menteesMatrix, mentorsMatrix):
        sendEmail(mentee, mentor)
     
main()